#ifndef UTILS_NAMEDPIPE_H
#define UTILS_NAMEDPIPE_H
#include <string>

namespace Utils
{

class NamedPipe
{
public:
    enum Mode { ModeRead = 0, ModeWrite = 1, ModeReadWrite = 2 };

    NamedPipe() = delete;

    static int open(const std::string &path, Mode mode, bool nonblock = true) noexcept;

    static int create(const std::string &path, Mode mode, int permissions, bool nonblock = true) noexcept;

    static int close(int fd) noexcept;

private:

    static int checkMode(const std::string &path, Mode mode);

    static int doOpen(const std::string &path, Mode mode, bool nonblock);

};

}

#endif // UTILS_NAMEDPIPE_H
