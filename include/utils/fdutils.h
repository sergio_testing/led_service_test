#ifndef UTILS_FDUTILS_H
#define UTILS_FDUTILS_H
#include <fcntl.h>
#include <unistd.h>

namespace Utils
{


class Closer
{
public:
    Closer(int fd)
        : m_fd(fd)
    { }

    ~Closer()
    {
        close();
    }

    void close() { if (m_fd >= 0) { ::close(m_fd); m_fd = -1; } }

    void release() { m_fd = -1; }

private:
    int m_fd;
};

}

#endif // UTILS_FDUTILS_H
