#ifndef NAMEDFIFOWRITER_H
#define NAMEDFIFOWRITER_H

#include <string>
#include <functional>
#include <utility>
#include <memory>

#include <boost/asio.hpp>

namespace InterProcess
{



class NamedFifoWriter : public std::enable_shared_from_this<NamedFifoWriter>
{
public:

    using NamedFifoWriterPtr = std::shared_ptr<NamedFifoWriter>;
    
    static NamedFifoWriterPtr create(boost::asio::io_service &ioService, std::string path, std::string msg)
    {
        return std::make_shared<NamedFifoWriter>(ioService, std::move(path), std::move(msg));
    }
    
    NamedFifoWriter(const NamedFifoWriter &rhs) = delete;
    NamedFifoWriter& operator=(const NamedFifoWriter &rhs) = delete;
    
    void send(unsigned int timeoutInSeconds);
    
    /**
     * @brief вызывать только через new
     * 
     * @param ioService ...
     * @param fifoPath ...
     * @param msg ...
     */
    NamedFifoWriter(boost::asio::io_service &ioService, std::string fifoPath, std::string msg);

private:

    bool doSend(unsigned int timeoutInSeconds);
    
private:
    const std::string m_fifoPath;
    const std::string m_msg;
    boost::asio::strand m_strand;
    boost::asio::posix::stream_descriptor m_stream;
    boost::asio::deadline_timer m_sendTimer;
    bool m_running{false};
};

} //end namespace

#endif // NAMEDFIFOWRITER_H
