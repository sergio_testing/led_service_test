#ifndef BASELEDCONTROLLER_H
#define BASELEDCONTROLLER_H

#include <stdexcept>
#include <string>

namespace LedsControl
{

/*!
 * базовый класс
 * все методы могут в случае ошибки возбуждать исключения
 * геттеры не константные на случай чтения из драйверов реального состояния с возможными ошибками получения параметров и кеширования их в самомо наследнике
 */
class BaseLedController
{
public:
    enum State
    {
        STATE_ON, STATE_OFF
    };
    virtual ~BaseLedController() {}
    
    virtual void setLedState(State state) = 0;
    
    virtual State ledState() = 0;
    
    virtual void setColor(const std::string &color) = 0;
    
    virtual std::string color() = 0;
    
    virtual void setRate(unsigned int rate) = 0;
    
    virtual unsigned int rate() = 0;
};

}

#endif // BASELEDCONTROLLER_H
