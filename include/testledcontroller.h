#ifndef TESTLEDCONTROLLER_H
#define TESTLEDCONTROLLER_H

#include <baseledcontroller.h>

namespace LedsControl
{

class TestLedController : public BaseLedController
{
public:
    
    TestLedController();
    
    virtual void setLedState(State state) override;
    
    virtual State ledState() override;
    
    virtual void setColor(const std::string &color) override;
    
    virtual std::string color() override;
    
    virtual void setRate(unsigned int rate) override;
    
    virtual unsigned int rate() override;
    
private:
    
    State m_state;
    std::string m_color;
    unsigned int m_rate;
    
};

}
#endif // TESTLEDCONTROLLER_H
