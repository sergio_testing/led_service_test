#ifndef COMMANDREADER_H
#define COMMANDREADER_H
#include <boost/asio.hpp>
#include <functional>
#include <utility>

namespace InterProcess
{

class NamedFifoReader
{
public:
    
    NamedFifoReader(boost::asio::io_service &ioService, std::string fifoPath);
    
    NamedFifoReader(const NamedFifoReader &rhs) = delete;
    NamedFifoReader& operator=(const NamedFifoReader &rhs) = delete;
    
    void start();
    
    template<typename Callable>
    void setMessageHandler(Callable && f)
    {
        m_lineHandler = std::forward<Callable>(f);
    }
    
    void clearMessageHandler()
    {
        m_lineHandler = nullptr;
    }
    
private:
    enum 
    { 
        RecreateToutSeconds = 1, 
        MaxMsgSize = 100
    };
    
private:
    
    void doStart();
    
    void shedule(unsigned int secondsDelay = RecreateToutSeconds);
    
    void handleRestartTimer(const boost::system::error_code &ec);
    
    void handleRead(const boost::system::error_code &ec, std::size_t bytesTransfered);
    
    void closeStream();
    
    void startRead();
    
    void handleMessage(const std::string &msg) const;
    
    
private:
    
    boost::asio::io_service &m_service;
    const std::string m_fifoPath;
    boost::asio::posix::stream_descriptor m_stream;
    boost::asio::streambuf m_inputbuf;
    boost::asio::deadline_timer m_restartTimer;
    bool m_running{false};
    std::function<void (const std::string &cmd)> m_lineHandler; //можно shared_ptr<handler> тогда как в c# потокобезопасно Events
};

}
#endif // COMMANDREADER_H
