#ifndef REQUESTPROCESSOR_H
#define REQUESTPROCESSOR_H

#include <string>
#include <memory>
#include <functional>
#include <baseledcontroller.h>
#include <commanddecoder.h>

namespace LedsControl
{

class RequestProcessor
{
public:
    
    RequestProcessor(std::unique_ptr<BaseLedController> ledController);
    
    void process(std::shared_ptr<Request> pRequest);
    
    template <typename Callable>
    void setAnswerSender(Callable &&func)
    {
        m_onAnswer = std::forward<Callable>(func);
    }
    
private:
    
    void sendAnswer(std::string &&toPath, std::string &&answer) const;
    
private:
    std::unique_ptr<BaseLedController> m_ledController;
    std::function<void (std::string &&, std::string &&)> m_onAnswer;
};

}

#endif // REQUESTPROCESSOR_H
