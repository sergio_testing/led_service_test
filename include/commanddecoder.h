#ifndef COMMANDDECODER_H
#define COMMANDDECODER_H
#include <string>
#include <map>
#include <iostream>
#include <utility>
#include <functional>
#include <memory>

namespace LedsControl
{
    
struct Request
{
    enum CmdType 
    {   
        CMD_INVALID, 
        CMD_SET_STATE, CMD_GET_STATE, 
        CMD_SET_COLOR, CMD_GET_COLOR,
        CMD_SET_RATE, CMD_GET_RATE
    };
    enum State
    {
        STATE_ON, STATE_OFF
    };

    CmdType m_type;
    std::string m_client;
    std::string m_color;
    unsigned int m_rate;
    State m_state;
};

class CommandDecoder
{
public:
    
    CommandDecoder();
    
    void handleMessage(const std::string& msg ) const;
    
    template<typename Callable>
    void setMessageHandler(Callable && f)
    {
        m_onRequest = std::forward<Callable>(f);
    }
    
    void clearMessageHandler()
    {
        m_onRequest = nullptr;
    }
    
    
private:
    
    void transferMessage(std::shared_ptr<Request> pRequest) const;
    
    std::shared_ptr<Request> setLedState(std::istream & str) const;
    std::shared_ptr<Request> getLedState(std::istream & str) const;
    std::shared_ptr<Request> setLedColor(std::istream & str) const;
    std::shared_ptr<Request> getLedColor(std::istream & str) const;
    std::shared_ptr<Request> setLedRate(std::istream & str) const;
    std::shared_ptr<Request> getLedRate(std::istream & str) const;

    typedef std::shared_ptr<Request> (CommandDecoder::*MemberFunc)(std::istream &) const;
    
    std::map<std::string,  MemberFunc> m_cmdsMap;
    std::function<void (std::shared_ptr<Request>)> m_onRequest;
};

}
#endif // COMMANDDECODER_H
