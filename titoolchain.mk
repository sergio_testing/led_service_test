SDK_PATH:=$(HOME)/ti-sdk-am335x-evm-07.00.00.00/linux-devkit
#SDK_PATH:=$(HOME)/ti-sdk-am335x-evm-06.00.00.00/linux-devkit
SDK_SYS:=i686-arago-linux
REAL_MULTIMACH_TARGET_SYS:=cortexa8hf-vfp-neon-3.8-oe-linux-gnueabi
#REAL_MULTIMACH_TARGET_SYS:=armv7ahf-vfp-neon-3.2-oe-linux-gnueabi
TOOLCHAIN_SYS:=arm-linux-gnueabihf

SDK_PATH_NATIVE:=$(SDK_PATH)/sysroots/$(SDK_SYS)
SDK_PATH_TARGET:=$(SDK_PATH)/sysroots/$(REAL_MULTIMACH_TARGET_SYS)
TOOLCHAIN_PREFIX:=$(SDK_PATH_NATIVE)/usr/bin/$(TOOLCHAIN_SYS)-
#export PATH=$SDK_PATH_NATIVE/usr/bin:$PATH
#export CPATH=$SDK_PATH_TARGET/usr/include:$CPATH

CC:=$(TOOLCHAIN_PREFIX)gcc
CXX:=$(TOOLCHAIN_PREFIX)g++
GDB:=$(TOOLCHAIN_PREFIX)gdb
CPP:="$(TOOLCHAIN_PREFIX)gcc -E"
NM:=$(TOOLCHAIN_PREFIX)nm
AS:=$(TOOLCHAIN_PREFIX)as
AR:=$(TOOLCHAIN_PREFIX)ar
RANLIB:=$(TOOLCHAIN_PREFIX)ranlib
OBJCOPY:=$(TOOLCHAIN_PREFIX)objcopy
OBJDUMP:=$(TOOLCHAIN_PREFIX)objdump
STRIP:=$(TOOLCHAIN_PREFIX)strip
#CONFIGURE_FLAGS="--target=arm-oe-linux-gnueabi --host=arm-oe-linux-gnueabi --build=i686-linux --with-libtool-sysroot=$SDK_PATH_TARGET"
CPPFLAGS:= -march=armv7-a -marm -mthumb-interwork -mfloat-abi=hard -mfpu=neon -mtune=cortex-a8 --sysroot=$(SDK_PATH_TARGET)
CFLAGS:=
CXXFLAGS:=
LDFLAGS:= --sysroot=$(SDK_PATH_TARGET)


