#use make [clean]project release=1 tiEL=1

PROJECT:= $(subst clean,,$(MAKECMDGOALS))
ifeq "$(PROJECT)" ""
# $(error error project is not set)
PROJECT:=led_service
endif

multithread=1

OBJ_DIR:=obj
BIN_DIR:=bin

include select_sdk.mk

CPPFLAGS +=
CFLAGS += -MD
CXXFLAGS += -MD -std=c++11
LDFLAGS += -lrt
DEFINES +=
ifndef release
DEFINES +=DEBUG
endif


ifdef multithread
CFLAGS += -pthread
CXXFLAGS += -pthread
LDFLAGS += -pthread
DEFINES += PTHREAD_USE
endif

ifdef release
OPT := -O1
else
OPT := -O0 -g
endif


include depends.mk

INCLUDES:=$(addprefix -I,$(INCLUDES))
LIBSPATHS:=$(addprefix -L,$(LIBSPATHS))
LIBS:=$(addprefix -l,$(LIBS))

CPPFLAGS +=$(addprefix -D,$(DEFINES))
CFLAGS +=$(OPT) $(INCLUDES) $(CPPFLAGS)
CXXFLAGS +=$(OPT) $(INCLUDES) $(CPPFLAGS)
LDFLAGS +=$(LIBSPATHS) $(LIBS)

include sources.mk


#OBJS = $(SRC:.cpp=.o)
COBJS:=$(patsubst %.c, %.o, $(filter %.c, $(SRC)))
CXXOBJS:=$(patsubst %.cpp, %.o, $(filter %.cpp, $(SRC)))
COBJS:=$(addprefix $(OBJ_DIR)/,$(COBJS))
CXXOBJS:=$(addprefix $(OBJ_DIR)/,$(CXXOBJS))
OBJS:= $(COBJS) $(CXXOBJS)
OBJ_DIRS:=$(addprefix $(OBJ_DIR)/, $(DIRS))

.PHONY: $(PROJECT)

$(PROJECT):all

all: $(OBJS) $(BIN_DIR)/$(PROJECT)

$(OBJS) : | directories

directories:
	@echo "create directories"
	mkdir -p $(BIN_DIR)
	mkdir -p $(OBJ_DIR)
	mkdir -p $(OBJ_DIRS)

$(OBJ_DIR)/%.o : %.cpp
	@echo "compile " $@ $(CXXFLAGS)
	$(CXX) -c $(CXXFLAGS)  -o $@ $<

$(OBJ_DIR)/%.o : %.c
	@echo "compile " $@ $(CFLAGS)
	$(CXX) -c $(CFLAGS)  -o $@ $<


$(BIN_DIR)/$(PROJECT): $(OBJS)
	@echo "link " $(PROJECT) $(LDFLAGS)
	$(CXX)  -o $@ $^ $(LDFLAGS)

.PHONY: clean$(PROJECT)

clean$(PROJECT): clean

.PHONY: clean

clean:
	@echo "clean " $(PROJECT)
	rm -rf $(OBJ_DIR)/*
	rm -f $(BIN_DIR)/$(PROJECT)

include $(wildcard $(addsuffix /*.d, $(OBJ_DIRS)))
