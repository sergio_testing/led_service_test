#include <string>
#include <exception>
#include <cstddef>
#include <cstdlib>
#include <array>
#include <iostream>
#include <locale>
#include <cstring>
#include <memory>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <errno.h>

#include <boost/asio.hpp>

#include <namedfiforeader.h>
#include <commanddecoder.h>
#include <testledcontroller.h>
#include <requestprocessor.h>
#include <namedfifowriter.h>

namespace ba = boost::asio;


static const char *defaultFifoPath = "led_service.fifo";

/**
 * @brief функция блокирует сигнал записи в открытый только на чтение pipe
 * 
 * @return void
 */
static void initSignals()
{
    sigset_t sset;
    sigemptyset(&sset);
    sigaddset(&sset, SIGPIPE);
    int res = pthread_sigmask(SIG_BLOCK, &sset, NULL); //ТОЛЬКО плохие параметры по моей ошибке
    if (res != 0)
    {
        std::cerr << "error block sigpipe" << strerror(errno) << '\n'; //strerror is not thread safe
    }
}

using namespace LedsControl;

int main(int argc, char *argv[])
{
    //TODO parsing options
    initSignals();
    std::locale::global(std::locale("C"));
    
    ba::io_service service;
    
    std::unique_ptr<BaseLedController> ledController{new TestLedController{}};
    
    RequestProcessor reqProcessor{std::move(ledController)};
    reqProcessor.setAnswerSender([&service](std::string &&path, std::string &&answer){
        try
        {
            auto writer = InterProcess::NamedFifoWriter::create(service, std::move(path), std::move(answer));
            writer->send(2);
        }
        catch (std::exception &e)
        {
            std::cerr << "answer sender start exception: " << e.what() << std::endl;
        }
    });
    
    CommandDecoder cmdDecoder;
    cmdDecoder.setMessageHandler( [&reqProcessor](std::shared_ptr<Request> req) {
        reqProcessor.process(std::move(req));
    });
    
    
    InterProcess::NamedFifoReader fifoReader(service, defaultFifoPath);
    fifoReader.setMessageHandler( [&cmdDecoder](const std::string &msg) {
        cmdDecoder.handleMessage(msg);
    });
    
    try
    {
        fifoReader.start();
        service.run();
    }
    catch (std::exception &e)
    {
        std::cerr << "exception: " << e.what() << std::endl;
        std::exit(2);
    }
    std::cout << "service if finished: " << std::endl; 
    return 0;
}

