#include <testledcontroller.h>
#include <iostream>

namespace LedsControl
{
    
TestLedController::TestLedController()
    : m_state(STATE_OFF),
    m_color{"red"},
    m_rate{}
{

}

void TestLedController::setLedState ( BaseLedController::State state )
{
    if (state != STATE_ON && state != STATE_OFF)
    {
        throw std::invalid_argument("invalid state");
    }
    m_state = state;
    std::cout << "TestLedController.setLedState: " << m_state << '\n';
}

BaseLedController::State TestLedController::ledState()
{
    std::cout << "TestLedController.ledState: " << m_state << '\n';
    return m_state;
}

void TestLedController::setColor ( const std::string& color)
{
    if (color != "red" && color != "blue" && color != "green")
    {
        throw std::invalid_argument("invalid color");
    }
    std::cout << "TestLedController.setColor: " << color << '\n';
    m_color = color;
}


std::string TestLedController::color()
{
    std::cout << "TestLedController.color: " << m_color << '\n';
    return m_color;
}

void TestLedController::setRate ( unsigned int rate )
{
    if (rate > 5)
    {
        throw std::invalid_argument("rate is great then 5");
    }
    std::cout << "TestLedController.setRate: " << rate << '\n';
    m_rate = rate;
}

unsigned int TestLedController::rate()
{
    std::cout << "TestLedController.rate: " << m_rate << '\n';
    return m_rate;
}



}// end namespace
