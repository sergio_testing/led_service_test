#include <utils/namedpipe.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <utils/fdutils.h>

namespace Utils
{


int NamedPipe::checkMode(const std::string &path, Mode mode)
{
    if (path.empty())
    {
        errno = EINVAL;
        return -2;
    }
    if ((int)mode < ModeRead || (int)mode > ModeReadWrite)
    {
        errno = EINVAL;
        return -3;
    }
    return 0;
}

static int osflags[] = {O_RDONLY, O_WRONLY, O_RDWR};

int NamedPipe::doOpen(const std::string &path, Mode mode, bool nonblock)
{
    int flags = osflags[mode] | O_CLOEXEC;
    if (nonblock)
    {
        flags |= O_NONBLOCK;
    }
    int tfd;
    do
    {
        tfd = ::open(path.c_str(), flags);
        if (tfd < 0)
        {
            if (errno != EINTR)
            {
                return -10;
            }
        }
    } while (tfd < 0);

    Utils::Closer cl(tfd);

    //проверяем что открыли fifo
    struct stat info;
    if (::fstat(tfd, &info) != 0)
    {
        return -11;
    }
    if (!S_ISFIFO(info.st_mode))
    {
	errno = EPIPE;
        return -12;
    }
    cl.release();
    return tfd;
}

int NamedPipe::open(const std::string &path, Mode mode, bool nonblock) noexcept
{
    int ret = checkMode(path, mode);
    if (ret != 0)
    {
        return ret;
    }
    return doOpen(path, mode, nonblock);
}

int NamedPipe::create(const std::string &path, Mode mode, int permissions, bool nonblock) noexcept
{
    int ret = checkMode(path, mode);
    if (ret != 0)
    {
        return ret;
    }
    ::unlink(path.c_str());
    if (mkfifo(path.c_str(), permissions) != 0)
    {
        return -4;
    }
    return doOpen(path, mode, nonblock);
}

int NamedPipe::close(int fd) noexcept
{
    return ::close(fd);
}


}
