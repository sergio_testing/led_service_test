#include <requestprocessor.h>
#include <iostream>

namespace LedsControl
{
RequestProcessor::RequestProcessor(std::unique_ptr< BaseLedController > ledController)
    : m_ledController{std::move(ledController)}
{
    if (!m_ledController)
    {
        throw std::invalid_argument("RequestProcessor ledController is null");
    }
}

void RequestProcessor::process(std::shared_ptr< Request > pRequest )
{
    std::string answer;
    answer.reserve(20);
    try
    {
        switch (pRequest->m_type)
        {
            case Request::CMD_SET_STATE:
            {
                m_ledController->setLedState(static_cast<BaseLedController::State>(pRequest->m_state)); //отображается один к одному
                answer = "OK\n";
                break;
            }
            case Request::CMD_GET_STATE:
            {
                auto state = m_ledController->ledState();
                if (state == BaseLedController::STATE_ON)
                {
                    answer = "OK on\n";
                }
                else
                {
                    answer = "OK off\n";
                }
                break;
            }
            case Request::CMD_SET_COLOR:
                m_ledController->setColor(pRequest->m_color);
                answer = "OK\n";
                break;
            case Request::CMD_GET_COLOR:
            {
                auto color = m_ledController->color();
                answer = "OK " + color + '\n';
                break;
            }
            case Request::CMD_SET_RATE:
                m_ledController->setRate(pRequest->m_rate);
                answer = "OK\n";
                break;
            case Request::CMD_GET_RATE:
            {
                auto rate = m_ledController->rate();
                answer = "OK " + std::to_string(rate) + '\n';
                break;
            }
            default:
                answer = "FAILED\n";
                break;
        }
    }
    catch (std::exception &e)
    {
        std::cout << "RequestProcessor::process exception: " << e.what() << '\n';
        answer = "FAILED\n";
    }
    std::cout << "process.answer:" << answer;
    sendAnswer(std::move(pRequest->m_client), std::move(answer));

}

void RequestProcessor::sendAnswer(std::string &&toPath, std::string &&answer) const
{
    if (m_onAnswer)
    {
        m_onAnswer(std::move(toPath), std::move(answer));
    }
}

} //end namespase
