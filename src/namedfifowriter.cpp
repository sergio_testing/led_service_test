#include <namedfifowriter.h>
#include <iostream>
#include <utils/fdutils.h>
#include <utils/namedpipe.h>
#include <boost/system/error_code.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

using namespace Utils;
namespace bs = boost::system;
namespace ba = boost::asio;

namespace InterProcess
{
    
NamedFifoWriter::NamedFifoWriter(boost::asio::io_service &ioService, std::string fifoPath, std::string msg)
    : m_fifoPath{std::move(fifoPath)},
    m_msg{std::move(msg)},
    m_strand{ioService},
    m_stream{ioService},
    m_sendTimer{ioService}
{
    
}

void NamedFifoWriter::send(unsigned int timeoutInSeconds)
{
    if (m_running)
    {
        return;
    }

    m_running = doSend(timeoutInSeconds);
}


bool NamedFifoWriter::doSend(unsigned int timeoutInSeconds)
{
    if (timeoutInSeconds < 1)
        timeoutInSeconds = 1;
    //открываем в неблокирующем режиме существующий для записи,если нет читателя сразу выйдем
    int fd = NamedPipe::open(m_fifoPath, NamedPipe::ModeWrite, true);
    if (fd < 0)
    {
        auto errCond = bs::system_category().default_error_condition(errno);
        std::cout << "error open fifo \"" << m_fifoPath << "\" : " <<  errCond.message() << '\n';
        return false;
    }
    Closer cl{fd};
    m_stream.assign(fd); //if error arg descriptor is not closed???
    cl.release();
    auto self = shared_from_this();
    ba::async_write(m_stream, ba::buffer(m_msg), m_strand.wrap([this, self](const bs::error_code& ec, std::size_t bytesTransfered)
    {
        if (!ec)
        {
            std::cout << "answer to " << m_fifoPath << " sended\n";
        }
        else
        {
            std::cout << "error send answer to " << m_fifoPath << " err:" << ec << '\n';
        }
        bs::error_code ignError;
        m_stream.close(ignError);
        m_sendTimer.cancel(ignError);
    }));
    //возможно только в случае если весь пайп будет забит и не читаем принимающей стороной
    m_sendTimer.expires_from_now(boost::posix_time::seconds(timeoutInSeconds));
    m_sendTimer.async_wait(m_strand.wrap([this, self](const bs::error_code &ec)
    {
        if (ec)
        {
            if (ec == ba::error::operation_aborted)
            {
                std::cout << "timer to " << m_fifoPath << " canceled\n";
            }
            else
            {
                std::cout << "timererr to " << m_fifoPath <<" : " << ec << '\n';
            }
        }
        if (m_stream.is_open())
        {
            std::cout << "cancel send to " << m_fifoPath << '\n';
            bs::error_code ignError;
            m_stream.close(ignError);
        }

    }));
    return true;
}

}// end namespace