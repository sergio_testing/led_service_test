#include <namedfiforeader.h>

#include <iostream>

#include <boost/date_time/posix_time/posix_time.hpp>

#include <utils/namedpipe.h>
#include <utils/fdutils.h>

namespace ba = boost::asio;
namespace bs = boost::system;

namespace InterProcess
{
    
using namespace Utils;

NamedFifoReader::NamedFifoReader(boost::asio::io_service &ioService, std::string fifoPath)
    : m_service{ioService},
    m_fifoPath{std::move(fifoPath)},
    m_stream{ioService},
    m_inputbuf{MaxMsgSize},
    m_restartTimer{ioService}
{
}

void NamedFifoReader::start()
{
    if (m_running)
    {
        return;
    }
    doStart();
    m_running = true;
}

void NamedFifoReader::doStart()
{
    //создаем новый fifo доступный владельцу на чтение&запись, процессы из группы могут писать
    int fd = NamedPipe::create(m_fifoPath, NamedPipe::ModeReadWrite, 0620, true);
    if (fd < 0)
    {
        auto errCond = bs::system_category().default_error_condition(errno);
        std::cout << "error create fifo \"" << m_fifoPath << "\" : " <<  errCond.message() << '\n';
        shedule();
        return;
    }
    Closer cl{fd};
    m_stream.assign(fd); //if error arg descriptor is not closed???
    cl.release();
    startRead();
}

void NamedFifoReader::shedule(unsigned int secondsDelay)
{
    if (secondsDelay < 1)
        secondsDelay = 1;
    m_restartTimer.expires_from_now( boost::posix_time::seconds(secondsDelay));
    m_restartTimer.async_wait([this](const bs::error_code &ec) { handleRestartTimer(ec); } );
}

void NamedFifoReader::handleRestartTimer(const boost::system::error_code& ec )
{
    if (!ec)
    {
        doStart();
    }
    else
    {
         std::cout << "timer err: " << ec << '\n'; //what???
         m_running = false;
    }
}


void NamedFifoReader::handleRead(const boost::system::error_code &ec, std::size_t bytesTransfered)
{
    if (ec)
    {
        std::cout << "error read: " << ec << '\n';
        closeStream();
        doStart();
        return;
    }
    std::istream istr{&m_inputbuf};
    std::string line;
    if (!std::getline(istr, line)) //маловероятно
    {
        std::cout << "error getline\n";
        closeStream();
        doStart();
        return;
    }
    std::cout << "line:" << bytesTransfered << ":" << line << "\n";
    try
    {
        handleMessage(line);
    }
    catch (...)
    {
        closeStream();
        m_running = false;
        throw;
    }
    startRead();
}

void NamedFifoReader::closeStream()
{
    bs::error_code ec;
    m_stream.close(ec);
    m_inputbuf.consume(m_inputbuf.max_size());
}

void NamedFifoReader::startRead()
{
    ba::async_read_until(m_stream, m_inputbuf, '\n', [this](const bs::error_code &ec, std::size_t bytesTransfered) 
    {
        handleRead(ec, bytesTransfered);
    });
}


void NamedFifoReader::handleMessage(const std::string &msg) const
{
    if (m_lineHandler)
    {
        m_lineHandler(msg);
    }
}

} //end namespace