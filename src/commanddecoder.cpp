#include <commanddecoder.h>
#include <sstream>


namespace LedsControl
{

CommandDecoder::CommandDecoder()
{
    m_cmdsMap["set-led-state"] = &CommandDecoder::setLedState;
    m_cmdsMap["get-led-state"] = &CommandDecoder::getLedState;
    m_cmdsMap["set-led-color"] = &CommandDecoder::setLedColor;
    m_cmdsMap["get-led-color"] = &CommandDecoder::getLedColor;
    m_cmdsMap["set-led-rate"] = &CommandDecoder::setLedRate;
    m_cmdsMap["get-led-rate"] = &CommandDecoder::getLedRate;
}

std::shared_ptr<Request> CommandDecoder::setLedState(std::istream & str) const
{
    std::shared_ptr<Request> preq{new Request{}};
    preq->m_type = Request::CMD_SET_STATE;
    std::string state;
    str >> state;
    if (!str)
    {
        preq->m_type = Request::CMD_INVALID;
    }
    else
    {
        if (state == "on")
        {
            preq->m_state = Request::STATE_ON;
        }
        else if (state == "off")
        {
            preq->m_state = Request::STATE_OFF;
        }
        else
        {
             preq->m_type = Request::CMD_INVALID;
        }
    }
    return preq;
}

std::shared_ptr<Request> CommandDecoder::getLedState(std::istream & str) const
{
    std::shared_ptr<Request> preq{new Request{}};
    preq->m_type = Request::CMD_GET_STATE;
    return preq;
}

std::shared_ptr<Request> CommandDecoder::setLedColor(std::istream & str) const
{
    std::shared_ptr<Request> preq{new Request{}};
    preq->m_type = Request::CMD_SET_COLOR;
    std::string color;
    str >> color;
    if (!str)
    {
        preq->m_type = Request::CMD_INVALID;
    }
    else
    {
//        if (color == "red" || color == "green" || color == "blue")
        {
            preq->m_color = std::move(color);
        }
//        else
//        {
//             preq->m_type = Request::CMD_INVALID;
//        }
    }
    return preq;
}

std::shared_ptr<Request> CommandDecoder::getLedColor(std::istream & str) const
{
    std::shared_ptr<Request> preq{new Request{}};
    preq->m_type = Request::CMD_GET_COLOR;
    return preq;
}

std::shared_ptr<Request> CommandDecoder::setLedRate(std::istream & str) const
{
    std::shared_ptr<Request> preq{new Request{}};
    preq->m_type = Request::CMD_SET_RATE;
    unsigned int rate;
    str >> rate;
    if (!str)
    {
        preq->m_type = Request::CMD_INVALID;
    }
    else
    {
//        if (rate <= 5)
        {
            preq->m_rate = rate;
        }
//        else
//        {
//             preq->m_type = Request::CMD_INVALID;
//        }
    }
    return preq;
}

std::shared_ptr<Request> CommandDecoder::getLedRate(std::istream & str) const
{
    std::shared_ptr<Request> preq{new Request{}};
    preq->m_type = Request::CMD_GET_RATE;
    return preq;
}


void CommandDecoder::handleMessage(const std::string& msg ) const
{
    std::istringstream ss(msg);
    std::string from, cmd;
    ss >> from;
    ss >> cmd;
    if (!ss)
    {
        std::cout << "err parse from and cmd\n";
        return;
    }
    auto it = m_cmdsMap.find(cmd);
    std::shared_ptr<Request> preq;
    if (it == m_cmdsMap.end())
    {
        preq.reset(new Request{});
        preq->m_type = Request::CMD_INVALID;
    }
    else
    {
        auto mf = it->second;
        preq = (this->*mf)(ss);
    }
    preq->m_client = std::move(from);
    transferMessage(std::move(preq));

}

void CommandDecoder::transferMessage( std::shared_ptr< Request > pRequest ) const
{
    if (m_onRequest)
    {
        m_onRequest(std::move(pRequest));
    }
}


}

