#!/bin/bash

server_fifo_path="led_service.fifo"

export LC_ALL=C


rm_path()
{
    local path_to_rm="$1"
    
    if [ -L "$path_to_rm" ]; then
        if ! rm "$path_to_rm"; then
            echo "can't rm_link $path_to_rm"
            return 1
        fi
    fi
    
    if [ -e "$path_to_rm" ]; then
        if ! rm -rf "$path_to_rm"; then
            echo "can't rm_rf $path_to_rm"
            return 2
        fi
    fi
    return 0
}

test_client_name()
{
    if [ -z "$1" ]; then
        echo "empty client name"
        return 1
    fi
    if ! [[ "$1" =~ ^[A-Za-z0-9]*$ ]]; then
        echo "invalid client name: $1"
        return 2
    fi
    return 0
}

create_fifo()
{
    local ff="$1"
    if ! [ -p $ff ]; then
        rm_path "$ff"
        if ! mkfifo -m 620 "$ff"; then
            echo "can't create client channel"
            return 1
        fi
    fi
    return 0
}

optstring=c:i:h

usage()
{
cat << EOF
usage: "$1" [ -options ] -i client_id -c command

Options:
    -i alpha numeric id of client
    -c command to send to server
    -h usage
commands:
set-led-state on | off
get-led-state
set-led-color red | green | blue
get-led-color
set-led-rate 0..5
get-led-rate    
EOF
}

client_id=
command_to_send=
while getopts $optstring opt; do
    case $opt in
        c)
            command_to_send="$OPTARG"
            ;;
        i)
            client_id="$OPTARG"
            ;;
        h)
            usage "$0"
            exit 1
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            usage "$0"
            exit 2
            ;;
    esac
done


if ! test_client_name "$client_id" >&2 ; then
    exit 2
fi

if [ -z "$command_to_send" ]; then
    echo "command to server is not set" >&2
    exit 2
fi


if ! create_fifo "$client_id"; then
    exit 2
fi

{
    if ! flock -n 200; then
        echo "client is locked"
        exit 2
    fi
    echo "send command to server..."
    if ! echo "$client_id $command_to_send" >"$server_fifo_path"; then
        echo "error send command"
        exit 2;
    fi
    echo "read answer from server"
    if ! read -t 5 server_answer <&200; then
        echo "error read answer from server"
        exit 2;
    fi
    echo "SERVER_ANSWER:$server_answer"
    exit 0
} 200<>"$client_id"

echo "error open client channel"
exit 3



